"""
This file aims to predict the dataset, you can read the instructions to run in the README
"""
import pandas as pd
import numpy as np
from joblib import load
import argparse


def input_fn(csv_data):
    """
    process the input data to deliver to the model. The same process that is done in data analysis notebook.

    args:
        csv_data: path to csv with the data to predict
    
    return:
        Pandas DataFrame with the rows processed and ready to input in the machine learning model
    """

    df_data = pd.read_csv(csv_data, index_col='index')

    #cleaning
    df_data.drop(['slope', 'cp', 'nar', 'fbs'], axis=1, inplace=True)
    df_data.dropna(inplace=True)
    if 'sex' in df_data.columns:
        df_data.drop('sex', axis=1, inplace=True)

    data_types = {
        'age': int,
        'trestbps': int,
        'chol': float,
        'restecg': int,
        'thalach': int,
        'exang': int,
        'oldpeak': float,
        'ca': int,
        'thal': int,
        'hc': int,
        'sk': int,
        'trf': float
    }
    df_data = df_data.astype(data_types, errors='ignore')

    norm_cols = ['age', 'trestbps', 'chol', 'thalach', 'oldpeak', 'ca', 'hc', 'sk', 'trf']

    categorical_data = {'restecg':[0,1,2], 'thal':[0,1,2,3]}
    df_data.reset_index(inplace=True)
    for k,v in categorical_data.items():
        cat = pd.Categorical(df_data[k], categories = v)
        df_data = pd.concat([df_data, pd.get_dummies(cat, prefix=k)], axis=1)
        df_data.drop(k, axis=1, inplace=True)
    df_data[norm_cols] = (df_data[norm_cols]-df_data[norm_cols].mean())/df_data[norm_cols].std()
    df_data.set_index('index',inplace=True)
    return df_data

def save(input_path, predictions):
    """
    Parser the path and save the results

    Args:
        input_path: the path to the input data
        predictions: the predictions returned by the model
    """

    
    dst = input_path.split('.')[0]+'_PREDICTIONS_Suayder_Milhomem_Costa.csv'
    np.savetxt(dst, predictions, fmt='%s')

def main(input_path):
    """
    args:
        input_path: path to the csv data used as input
    """

    input_data = input_fn(input_path)
    model = load('sex_predictor.joblib')
    pred = model.predict(input_data)

    input_data['sex'] = pred
    input_data = input_data.reindex(list(range(input_data.index.min(),input_data.index.max()+1)),fill_value='nan')
    save(input_path, input_data['sex'].to_numpy())

if __name__=="__main__":


    parser = argparse.ArgumentParser(description='Sex predictor')
    parser.add_argument('--input_file')

    args = parser.parse_args()
    main(args.input_file)